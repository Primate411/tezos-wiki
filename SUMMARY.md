# Summary

* [Introduction to the Tezos Wiki](README.md)
* [Tezos Whitepaper](files/whitepaper.md)

### Tezos Basics

* [What is Tezos?](files/basics.md#intro)
* [What makes Tezos unique?](files/basics.md#unique)
* [Tezos Nomenclature](files/basics.md#nomenclature)

### Proof-of-Stake
* [What is Proof-of-Stake?](files/proofofstake.md#intro)
* [The Tezos consensus algorithm](files/proofofstake.md#consensus)

### Baking
* [What is baking?](files/baking.md#what)
* [What is delegating?](files/baking.md#delegate)
* [Should I bake or delegate?](files/baking.md#bakeordelegate)
* [Selecting a baker](files/baking.md#bakerselection)

### Tezos Governance
* [What is Self Amendment?](files/self-amendment.md#introduction)
* [How does it work?](files/self-amendment.md#how)
* [Off-chain Governance](files/self-amendment.md#offchain)

### Smart Contracts in Tezos
* [Introduction](files/language.md#faq)
* [Michelson Language](files/language.md#michelson)
* [High-Level Smart Contract Languages](files/language.md#high-level-languages)
* [Language Resources](files/language.md#resources)

### Formal Verification
* [Introduction](files/formal-verification.md#intro)
* [Michelson and Coq](files/formal-verification.md#coq)

### Future Developments
* [Privacy](files/future.md#intro)
* [Consensus](files/future.md#consensus)
* [Layer 2](files/future.md#layer2)
* [Governance](files/future.md#governance)

### Built on Tezos
* [Tezos Projects](https://tezosprojects.com)

### Resources
* [Wallets](files/resources.md#wallet)
* [Block Explorer](files/resources.md#explorer)
* [Baking Services](files/resources.md#baking)
