---
layout: post
title:  "Resources"
date:   2019-01-07 12:14:18
---
Resources
===========

# ꜩ Tezos Wallets {#wallet}
- [Kukai (Web)](https://kukai.app/)
- [Thanos (Browser Extension)](https://thanoswallet.com)
- [Magma (iOS and Android)](https://magmawallet.io) 
- [Galleon (Desktop)](https://galleon-wallet.tech/)

- [AirGap](https://airgap.it)
- [Atomex](https://atomex.me/)
- [Exodus Wallet](http://exodus.io)
- [Ledger Live](https://ledger.com/ledger-live/download)
- [Simple Staking](https://simplestaking.com)
- [Trust Wallet](http://trustwallet.com)

# Block Explorers {#explorer}
- [TZKT](https://tzkt.io)
- [Better Call Dev - "Michelson Contract Explorer"](https://better-call.dev/)
- [TZStats](https://tzstats.com/)
- [Arronax](https://arronax-beta.cryptonomic.tech/)
- [Tezblock](https://tezblock.io/)


# Baker Rating Services {#baking}
- [Baking Bad](https://baking-bad.org)
- [Tezos Nodes](https://www.tezos-nodes.com/) [iOS](https://apps.apple.com/us/app/tezos-nodes/id1517012548?l=&ls=1) [Android](https://play.google.com/store/apps/details?id=com.tezosnodes.tezosnodes)


# List of Tezos Groups {#foundation}

- [Tezos Foundation](https://tezos.foundation/)
- [Tezos Commons Foundation](https://tezoscommons.org/)
- [Tezos Japan](https://twitter.com/TezosJapan)
- [Tezos Korea](https://tezoskoreacommunity.org/)
- [Tezos Rio](https://tezos.rio/)
- [Tezos Korea Foundation](http://tezoskorea.foundation/)
- [Tezos Southeast Asia](https://www.tezos.org.sg/)
- [Tezos Montreal](https://tezosmtl.com/)
- [Tezos OCaml Michelson Institute](https://tomi.institute/)
- [Tezos Detroit](https://twitter.com/TezosD)
- [Tezos Luxembourg](https://tezos.lu/)
- [Tezos Turkey](https://twitter.com/tezosturkey)
- [Tezos Geneva](https://twitter.com/TezosGeneva)
